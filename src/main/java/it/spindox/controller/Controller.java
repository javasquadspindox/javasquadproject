package it.spindox.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import it.spindox.entity.Person;
import it.spindox.request.PersonRequest;
import it.spindox.service.MyService;

@RestController
@RequestMapping("/test")
public class Controller {

	@Autowired
	private MyService service;



	@GetMapping("/person/{id}")
	public Person getPerson(@PathVariable Integer id){
		return service.getPerson(id);
	}

	@GetMapping("/personh/{id}")
	public Person getCustomPerson(@PathVariable Integer id){
		return service.ricercaPerson(id);
	}

	@GetMapping("/person-name/{id}")
	public String getPersonName(@PathVariable Integer id){
		return service.ricercaNamePerson(id);
	}

	@GetMapping("/person-by-name/{name}")
	public List<Person> getPersonName(@PathVariable String name){
		return service.ricercaByName(name);
	}

	@GetMapping("/people")
	public List<Person> getPeople(@RequestBody PersonRequest req){
		return service.getPeople(req);
	}




	@PostMapping("/insert")
	public Person insertPerson(@RequestBody Person p){
		return service.insertPerson(p);
	}

	@PostMapping("/inserth")
	public void insertHPerson(@RequestBody Person p){
		service.insertHPerson(p);
	}


	@PutMapping("/update/{id}")
	public Person updatePerson(@PathVariable Integer id, @RequestBody Person p){
		return service.updatePerson(p, id);
	}

	@PutMapping("/update")
	public Person update(@RequestBody PersonRequest req){
		return service.update(req);
	}

	@DeleteMapping("/delete/{id}")
	public void delete(@PathVariable Integer id){
		service.deletePerson(id);
	}


	@DeleteMapping("/remove")
	public void remove(@RequestBody PersonRequest req){
		service.remove(req);
	}



}
