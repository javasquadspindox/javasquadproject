package it.spindox.request;

import it.spindox.entity.Person;

public class PersonRequest {
	
	private Person person;

	public Person getPerson() {
		return person;
	}

	public void setPerson(Person person) {
		this.person = person;
	}
	

}
