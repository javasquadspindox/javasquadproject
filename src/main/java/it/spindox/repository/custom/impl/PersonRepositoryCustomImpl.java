package it.spindox.repository.custom.impl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.transaction.Transactional;

import it.spindox.entity.Person;
import it.spindox.repository.custom.PersonRepositoryCustom;
import it.spindox.request.PersonRequest;
import it.spindox.utils.BeanUtilities;

public class PersonRepositoryCustomImpl implements PersonRepositoryCustom {

	// GET
	@Override
	public Person ricercaById(Integer idPerson) {

		// Connessione DB
		EntityManager entityManager = ((EntityManager) BeanUtilities.getBean(EntityManager.class));

		// Creazione del creatore
		CriteriaBuilder builder = entityManager.getCriteriaBuilder();

		// Creazione della query (come se fosse un container che poi ospiterà la
		// query vera e propria) --- SELECT * + predisposizione per il FROM
		// PERSONA
		CriteriaQuery<Person> query = builder.createQuery(Person.class);

		// Aggiungiamo il FROM PERSONA
		Root<Person> root = query.from(Person.class);

		// Creiamo un array di predicati= condizioni della query
		List<Predicate> predicates = new ArrayList<>();

		// aggiungiamo all'array dei predicate la condizione che vogliamo,
		// dentro il get() bisogna mettere il nome che si ha nel DB
		predicates.add(builder.equal(root.get("id"), idPerson));

		// Aggiunge i predicates/condizioni alla query
		query.where(builder.and(predicates.toArray(new Predicate[predicates.size()])));

		// Esegue la query
		List<Person> lista = entityManager.createQuery(query).getResultList();

		// per gestione errori
		if (lista == null || lista.isEmpty()) {
			return null;
		}
		return lista.get(0);
	}

	@Override
	public String ricercaNameById(Integer idPerson) {

		EntityManager entityManager = ((EntityManager) BeanUtilities.getBean(EntityManager.class));
		CriteriaBuilder builder = entityManager.getCriteriaBuilder();

		CriteriaQuery<String> query = builder.createQuery(String.class);
		Root<Person> root = query.from(Person.class);
		List<Predicate> predicates = new ArrayList<>();
		predicates.add(builder.equal(root.get("id"), idPerson));

		query.select(root.get("name"));
		query.where(builder.and(predicates.toArray(new Predicate[predicates.size()])));

		List<String> lista = entityManager.createQuery(query).getResultList();
		if (lista == null || lista.isEmpty()) {
			return null;
		}
		return lista.get(0);
	}

	@Override
	public List<Person> ricercaByName(String name) {

		EntityManager entityManager = ((EntityManager) BeanUtilities.getBean(EntityManager.class));
		CriteriaBuilder builder = entityManager.getCriteriaBuilder();

		CriteriaQuery<Person> query = builder.createQuery(Person.class);
		Root<Person> root = query.from(Person.class);
		List<Predicate> predicates = new ArrayList<>();
		predicates.add(builder.equal(root.get("name"), name));

		query.where(builder.and(predicates.toArray(new Predicate[predicates.size()])));
		query.orderBy(builder.desc(root.get("surname")));

		List<Person> lista = entityManager.createQuery(query).getResultList();
		if (lista == null || lista.isEmpty()) {
			return null;
		}
		return lista;
	}

	// POST
	@Override
	@Transactional
	public void insertHPerson(Person p) {
		EntityManager entityManager = ((EntityManager) BeanUtilities.getBean(EntityManager.class));
		entityManager.merge(p);
	}

	@Override
	public List<Person> getPeople(PersonRequest req) {
		EntityManager entityManager = ((EntityManager) BeanUtilities.getBean(EntityManager.class));
		CriteriaBuilder builder = entityManager.getCriteriaBuilder();

		CriteriaQuery<Person> query = builder.createQuery(Person.class);
		Root<Person> root = query.from(Person.class);
		List<Predicate> predicates = new ArrayList<>();

		if (req != null && req.getPerson() != null) {
			if (req.getPerson().getName() != null)
				predicates.add(builder.equal(root.get("name"), req.getPerson().getName()));
			if (req.getPerson().getSurname() != null)
				predicates.add(builder.equal(root.get("surname"), req.getPerson().getSurname()));
			if (req.getPerson().getAge() != null)
				predicates.add(builder.equal(root.get("age"), req.getPerson().getAge()));
			if (req.getPerson().getId() != null)
				predicates.add(builder.equal(root.get("id"), req.getPerson().getId()));
		}
		query.where(builder.and(predicates.toArray(new Predicate[predicates.size()])));

		List<Person> lista = entityManager.createQuery(query).getResultList();
		if (lista == null || lista.isEmpty()) {
			return null;
		}
		return lista;
	}

	@Override
	@Transactional
	public void remove(PersonRequest req) {
		EntityManager entityManager = ((EntityManager) BeanUtilities.getBean(EntityManager.class));
		if (req != null && req.getPerson() != null) {
			Person p = req.getPerson();
			if (p.getId() != null) {
				p = entityManager.find(Person.class, p.getId());
				if (p != null)
					entityManager.remove(p);
			}

		}
	}

	@Override
	@Transactional
	public Person update(PersonRequest req) {
		EntityManager entityManager = ((EntityManager) BeanUtilities.getBean(EntityManager.class));
		if (req != null && req.getPerson() != null && req.getPerson().getId() != null) {
			entityManager.merge(req.getPerson());
			return req.getPerson();
		}
		return null;
	}

}
