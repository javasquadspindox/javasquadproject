package it.spindox.repository.custom;

import java.util.List;

import it.spindox.entity.Person;
import it.spindox.request.PersonRequest;

public interface PersonRepositoryCustom {

	public Person ricercaById(Integer idPerson);

	String ricercaNameById(Integer idPerson);

	List<Person> ricercaByName(String name);

	void insertHPerson(Person p);
	
	List<Person> getPeople(PersonRequest req);
	
	public void remove(PersonRequest req);

	Person update(PersonRequest req);
}
