package it.spindox.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import it.spindox.entity.Person;
import it.spindox.repository.custom.PersonRepositoryCustom;

public interface PersonRepository extends JpaRepository<Person, Integer>, PersonRepositoryCustom{
	

}
