package it.spindox.response;

public class GenericResponse {
	
	private String result;
	private String description;
	
	public String getResult() {
		return result;
	}
	public void setResult(String esito) {
		this.result = esito;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
	

}
