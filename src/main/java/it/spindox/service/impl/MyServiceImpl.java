package it.spindox.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import it.spindox.entity.Person;
import it.spindox.repository.PersonRepository;
import it.spindox.request.PersonRequest;
import it.spindox.service.MyService;

@Service
public class MyServiceImpl implements MyService {

	@Autowired
	PersonRepository personRepository;
	
	
	@Override
	public Person getPerson(Integer id){
		return personRepository.findById(id).orElse(null);
	}
	
	@Override
	public Person insertPerson(Person p){
		return personRepository.save(p);
	}
	
	@Override
	public void deletePerson(Integer id){
		personRepository.deleteById(id);
	}

	@Override
	public Person updatePerson(Person p, Integer id) {
//		Person pold = repository.findById(id).get();
		p.setId(id);
		return personRepository.save(p);
	}

	@Override
	public Person ricercaPerson(Integer id) {
		return personRepository.ricercaById(id);
	}

	@Override
	public String ricercaNamePerson(Integer id) {
		return personRepository.ricercaNameById(id);
	}

	@Override
	public List<Person> ricercaByName(String name) {
		return personRepository.ricercaByName(name);
	}

	@Override
	public void insertHPerson(Person p) {
		personRepository.insertHPerson(p);
	}

	@Override
	public List<Person> getPeople(PersonRequest req) {
		return personRepository.getPeople(req);
	}

	@Override
	public void remove(PersonRequest req) {
		personRepository.remove(req);
		
	}

	@Override
	public Person update(PersonRequest req) {
		return personRepository.update(req);
	}

	

}
