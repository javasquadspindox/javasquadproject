package it.spindox.service;

import java.util.List;

import it.spindox.entity.Person;
import it.spindox.request.PersonRequest;

public interface MyService {
		
	public Person getPerson(Integer id);
	
	public Person insertPerson(Person p);
	
	public void deletePerson(Integer id);
	
	public Person updatePerson(Person p, Integer id);

	public Person ricercaPerson(Integer id);

	public String ricercaNamePerson(Integer id);

	public List<Person> ricercaByName(String name);

	public void insertHPerson(Person p);

	public List<Person> getPeople(PersonRequest req);

	public void remove(PersonRequest req);

	public Person update(PersonRequest req);
	

}
